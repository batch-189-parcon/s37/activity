const jwt = require('jsonwebtoken');

const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {}) 
	//(<payload>,secretkey, {})
}

module.exports.verify = (request, response, next) => {
	let token = request.headers.authorization;
//no condition mentioned data will show error
// may laman yung token can proceed
	if(typeof token !== "undefined") {
		// console.log(token);
		//Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyYzI4YzY1OTJlNjIyODM0MTY0MGY1NSIsImVtYWlsIjoiY3BsaWxhZ2FuQG1haWwuY29tIiwiaXNBZG1pbiI6ZmFsc2UsImlhdCI6MTY1Njk5MDIzOH0.027EIIRO-RHmVzhhxeR--quB5ahc09_ioZ64S9YaRfw
		//to slice of (Bearer_) to get token only
		token = token.slice(7, token.length)

		//secret set by variable and must match
		return jwt.verify(token,secret, (error, data) => {
			if(error){
				return response.send({auth: "failed"})
			} else {

				next()
			}
		})
	} else {
		return response.send({auth: "failed"})
	}
}


module.exports.decode = (token) => {
	//Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyYzI4YzY1OTJlNjIyODM0MTY0MGY1NSIsImVtYWlsIjoiY3BsaWxhZ2FuQG1haWwuY29tIiwiaXNBZG1pbiI6ZmFsc2UsImlhdCI6MTY1Njk5MDIzOH0.027EIIRO-RHmVzhhxeR--quB5ahc09_ioZ64S9YaRfw
	console.log(token)

	if(typeof token !== "undefined") {
		//eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyYzI4YzY1OTJlNjIyODM0MTY0MGY1NSIsImVtYWlsIjoiY3BsaWxhZ2FuQG1haWwuY29tIiwiaXNBZG1pbiI6ZmFsc2UsImlhdCI6MTY1Njk5MDIzOH0.027EIIRO-RHmVzhhxeR--quB5ahc09_ioZ64S9YaRfw
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return null
			} else {
				//since an object, need to get info of payload
				//decode - get specific details from token
				return jwt.decode(token, {complete: true}).payload
			}
		})
	} else {
		return null
	}
}