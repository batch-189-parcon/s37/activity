const express = require('express')
const router = express.Router()

const courseController = require('../controllers/courseController')
const auth = require("../auth");



//Route for creating a course
router.post('/', auth.verify, (request, response) => {

	const adminData = auth.decode(request.headers.authorization)

	if(adminData.isAdmin) {
		courseController.addCourse(request.body).then(resultFromController => response.send(resultFromController))
	} else{
		response.send("Admin function only!")
	}
})


router.get("/all", auth.verify, (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	courseController.getAllcourses(userData).then(resultFromController => response.send(resultFromController))
})

//Route for retrieving all active courses
router.get("/", (request, response) => {
	courseController.getAllActive().then(resultFromController => response.send(resultFromController))
})

//Route for retrieving a specific course
//localhost:4000/courses/1231244245
// */:parameterName
router.get("/:courseId", (request, response) => {
	console.log(request.params)

	courseController.getCourse(request.params).then(resultFromController => response.send(resultFromController))
})

// Route for updating a course
router.put("/:courseId", auth.verify, (request, response) => {
	courseController.updateCourse(request.params, request.body).then(resultFromController => response.send(resultFromController))
})

router.put("/:courseId", auth.verify, (request, response) => {
	courseController.courseDeactivate(request.params, request.body).then(resultFromController => response.send(resultFromController))
})




module.exports = router;