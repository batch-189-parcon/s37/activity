const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require("../auth");

router.post("/checkEmail", (request, response)=> { 
	userController.checkEmailExists(request.body).then(resultFromController => response.send(resultFromController));
})

router.post("/register", (request, response) => {
	userController.registeredUser(request.body).then(resultFromController => response.send(resultFromController))
})


router.post("/login", (request, response) => {
	userController.loginUser(request.body).then(resultFromController => response.send(resultFromController))

})

router.get('/details', auth.verify, (request, response) => {
//Provides the user's ID for the getProfile controller method
//Need to verify if user is logged-in
	//argument from req.headers
	const userData = auth.decode(request.headers.authorization)
	// const isAdminData = auth.decode(request.headers.authorization)
	//console.log - to check on terminal details
	console.log(userData)
	// console.log(isAdminData)
	/*
	{
	 id: '62c28c6592e6228341640f55',
  	email: 'cplilagan@mail.com',
  	isAdmin: false,
  	iat: 1656990238
	}
	*/

	// console.log(request.headers.authorization)

	//dot notation since in an object
	//isAdmin - object to check condition
	userController.getProfile({id: userData.id}).then((resultFromController) => response.send(resultFromController))
})

// router.post("/enroll", auth.verify, (request, response) => {

// 	let data = {
// 		userId: request.body.userId,
// 		courseId: request.body.courseId
// 	}

// 	userController.enroll(data).then(resultFromController => response.send(resultFromController))
// })

router.post("/enroll", auth.verify, (request, response) => {
	
	let data = {
		userId: auth.decode(request.headers.authorization).id,
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		courseId: request.body.courseId
	}

	userController.enroll(data).then(resultFromController => response.send(resultFromController))
})


module.exports = router;