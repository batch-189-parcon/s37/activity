const Course = require('../models/course');
const User = require('../models/user')

//Create a new course
module.exports.addCourse = (requestBody) => {
	let newCourse = new Course({
		name: requestBody.name,
		desciption: requestBody.description,
		price: requestBody.price
	})

	return newCourse.save().then((course, error) => {

		if(error){

			return false

		} else {

			return true

		}
	})

}

module.exports.getAllcourses = async (data) => {
	if (data.isAdmin) {
		return Course.find({}).then(result => {
			return result
		})
	} else {
		return false
	}

}

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	})
}

module.exports.getCourse = (requestParams) => {
	return Course.findById(requestParams.courseId).then(result =>{
		return result
	})
}

module.exports.updateCourse = (requestParams, requestBody) => {
	let updatedCourse = {
		name: requestBody.name,
		desciption: requestBody.description,
		price: requestBody.price
	}

	return Course.findByIdAndUpdate(requestParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}


module.exports.courseDeactivate = (requestParams, requestBody) => {
	let deactiveCourse = {
		isActive: requestBody.isActive
	}

	return Course.findByIdAndUpdate(requestParams.courseId, deactiveCourse).then((course, error)=>{
		if(error){
			return false
		} else {
			return true
		}
	})
}


