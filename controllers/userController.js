const User = require('../models/user');
const Course = require('../models/course');
const bcrypt = require('bcrypt');
const auth = require('../auth');

//Check if the email already exists
module.exports.checkEmailExists = (requestBody) => {
	return User.find({email: requestBody.email}).then(result => {
		if (result.length > 0) {
			return true
		} else {
			return false
		}
	})
}

//User Registration
module.exports.registeredUser = (requestBody) => {
	let newUser = new User ({
		firstName: requestBody.firstName,
		lastName: requestBody.lastName,
		email: requestBody.email,
		mobileNo: requestBody.mobileNo,
		password: bcrypt.hashSync(requestBody.password, 10) 
		//hashSync(<dataToBeHash>, <saltRound>)
	})

	return newUser.save().then((user, error) =>{
		if(error) {
			return false
		} else {
			return true
		}
	})
}

//User Authentication
module.exports.loginUser = (requestBody) => {
	return User.findOne({email: requestBody.email}).then(result => {
		if (result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)
			//compareSync (<dataToCompare>, <encryptedPassword>) // will return true/false
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

//Change the display only from an object.
module.exports.getProfile = (requestBody) => {
	return User.findOne({_id: requestBody.id}).then(result => {
		console.log(result)
		if(result == 0) {
			return false
		} else {
			result.password = ""
			return result
		}
	})

}



//Enroll User to A Class
//async & await - wait for the other process of (isUser and isCourse)
module.exports.enroll = async (data) => {

	if (data.isAdmin == true) {
		return "Admin not allowed"
	} else {
		let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId})

		return user.save().then((user, error) =>{

			if(error){
				return false

			} else {

				return true
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({userId: data.userId})

		return course.save().then((course, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})

	if (isUserUpdated && isCourseUpdated) {
		return true

	} else {

		return false
	}
}
}

	